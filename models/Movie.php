<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movie".
 *
 * @property integer $id
 * @property string $movieName
 * @property string $genre
 * @property integer $MinimumAge
 * @property integer $FilmScore
 */
class Movie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['movieName', 'genre', 'MinimumAge', 'FilmScore'], 'required'],
            [['MinimumAge', 'FilmScore'], 'integer'],
            [['movieName', 'genre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'movieName' => 'Movie Name',
            'genre' => 'Genre',
            'MinimumAge' => 'Minimum Age',
            'FilmScore' => 'Film Score',
        ];
    }
}
