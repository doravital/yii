<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movie`.
 */
class m170610_111615_create_movie_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('movie', [
            'id' => $this->primaryKey(),
			'movieName' => $this->string()->notNull(),
			'genre' => $this->string()->notNull(),
			'MinimumAge' => $this->integer()->notNull(),
			'FilmScore' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movie');
    }
}
