<?php

use yii\db\Migration;

class m170527_083808_create_table_student extends Migration
{
    public function up()
    {
		$this->createTable('student', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'identitynumber' => $this->integer()->notNull(),
			'age' => $this->integer()->notNull(),
		
        ]);
    }

    public function down()
    {
       $this->dropTable('student');

    }

}
